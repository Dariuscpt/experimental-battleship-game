#Change Ship Names for Length - Capital if hit, Lowercase if not

#importing modules
import os
from random import randint
from time import sleep

#initializing board
Board = []
ShipLength = 2

for x in range(10):
    Board.append(["[ ]"] * 10)

def print_board(board):
    RowLetters = "[ ] "
    for x in range(65, 75):
        RowLetters = (RowLetters +("[" + chr(x) + "] "))
    print (RowLetters)
    x = -1
    for row in board:
        x = x + 1
        print(("[" + str(x) + "] ") + ((" ").join(row)))

print_board(Board)

while True:
    ErrorChecked = False
    
    #User ShipRotation Input Verification
    while True:
        print("Would you like to place your ship horizontally, vertically or diagonally? (This ship is", str(ShipLength), "tiles long.)")
        ShipRot = input("Enter Here: ").upper()
        if ShipRot[:1] == "H" or ShipRot[:1] == "V":
            break
        elif ShipRot[:1] == "D":
            while True:
                RotationType = input("Would you like the ship to move [T]op left to bottom right or [B]ottom left to top right? (Input [B/T])\nEnter Here:").upper()
                if RotationType[:1] == "T" or RotationType[:1] == "B":
                    break
                else:
                    print("Incorrect input, please try again.\n")
            break
        else:
            print("Incorrect input, please try again.\n")

    #User Column Input
    while True:
        UserCol = input("\nWhich column would you like to place the ship on? (Select the letter that the bottom left corner of the ship will be placed at)\nEnter Here: ").upper()
        if UserCol.isalpha() == False or ((ord(UserCol) - 65) <= 9 and (ord(UserCol)-65) >= 0) == False:
            print("Incorrect input, please try again.")
        else:
            UserCol = ord(UserCol) - 65
            break

    #User Row Input Verification
    while True:
        try:
            UserRow = int(input("\nWhich row would you like to place the ship on? (Select the number that the bottom left corner of the ship will be placed at)\nEnter Here: "))
            break
        except:
            print("Incorrect input, please try again.")

    #Final Input Verification
    while True:
        if ShipRot[:1] == "H":
            if UserCol + ShipLength > 9:
                print("That would go out of bounds, please try again.\n")
            else:
                ErrorChecked = True
                
        elif ShipRot[:1] == "V":
            if UserRow - ShipLength < -1:
                print("That would go out of bounds, please try again.\n")
            else:
                ErrorChecked = True
                
        elif ShipRot[:1] == "D":
            if RotationType[:1] == "B":
                if UserCol + ShipLength > 10 or UserRow - ShipLength < -1:
                    print("That would go out of bounds, please try again.\n")
                else:
                    ErrorChecked = True
            elif RotationType[:1] == "T":
                if UserCol + ShipLength > 10 or UserRow + ShipLength > 10:
                    print("That would go out of bounds, please try again.\n")
                else:
                    ErrorChecked = True
        break

    if ErrorChecked == False:
        continue

    ErrorChecked = True
    #Ship Placement Overlap Check
    if ShipRot[:1] == "H":
        for x in range(0, ShipLength):
            if ((Board[UserRow])[UserCol + x]) == "[B]":
                ErrorChecked = False
                print ((Board[UserRow])[UserCol + x])
                
    elif ShipRot[:1] == "V":
        for x in range(0, ShipLength):
            if ((Board[UserRow - x])[UserCol]) == "[B]":
                ErrorChecked = False
                print ((Board[UserRow])[UserCol + x])

    elif ShipRot[:1] == "D":
        if RotationType[:1] == "B":
            for x in range(0, ShipLength):
                if ((Board[UserRow - x])[UserCol + x]) == "[B]":
                    ErrorChecked = False
        elif RotationType[:1] == "T":
            for x in range(0, ShipLength):
                if ((Board[UserRow + x])[UserCol + x]) == "[B]":
                    ErrorChecked = False

    if ErrorChecked == False:
        print("That would overlap a previously placed ship, please try again.")
        continue

    #Ship Placement
    if ShipRot[:1] == "H":
        for x in range(0, ShipLength):
            Board[UserRow].pop(UserCol + x)
            Board[UserRow].insert((UserCol + x), "[B]")
            
    elif ShipRot[:1] == "V":
        for x in range(0, ShipLength):
            Board[UserRow - x].pop(UserCol)
            Board[UserRow - x].insert((UserCol), "[B]")
            
    elif ShipRot[:1] == "D":
        if RotationType[:1] == "B":
            for x in range(0, ShipLength):
                Board[UserRow - x].pop(UserCol + x)
                Board[UserRow - x].insert((UserCol + x), "[B]")
        elif RotationType[:1] == "T":
            for x in range(0, ShipLength):
                Board[UserRow + x].pop(UserCol + x)
                Board[UserRow + x].insert((UserCol + x), "[B]")
                
    ShipLength += 1    
    print_board(Board)
    
  # print ((Board[UserRow + x])[UserCol + x])
